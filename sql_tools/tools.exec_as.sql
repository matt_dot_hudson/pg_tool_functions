-- execute dynamic with temporary role change
CREATE OR REPLACE FUNCTION tools.exec_as(
  p_command text
  , p_role text
) RETURNS void LANGUAGE plpgsql AS
$exec_as$
DECLARE
BEGIN
  PERFORM tools.assert( p_role IS NOT NULL, 'tools.exec_as: p_role may not be NULL');
  EXECUTE 'SET LOCAL ROLE ' || quote_ident(p_role);
  PERFORM tools.exec(p_command);
END;
$exec_as$;
