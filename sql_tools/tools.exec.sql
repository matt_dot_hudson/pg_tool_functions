-- execute dynamic
CREATE OR REPLACE FUNCTION tools.exec(
  p_command text
) RETURNS void SET search_path FROM CURRENT LANGUAGE plpgsql AS 
$exec$
DECLARE
BEGIN
  PERFORM tools.assert(p_command IS NOT NULL, 'tools.exec: p_command may not be NULL');
  EXECUTE p_command;
END;
$exec$;
