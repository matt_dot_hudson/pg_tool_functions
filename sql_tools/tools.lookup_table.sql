CREATE OR REPLACE FUNCTION tools.lookup_table(
    p_schema_name text
    , p_table_name text
    , p_object_name text
) RETURNS void LANGUAGE plpgsql AS $lookup_table$
DECLARE
  v_params text[] := NULL;
BEGIN
    PERFORM tools.assert( p_schema_name IS NOT NULL, 'tools.lookup_table: p_schema_name must not be NULL' );
    PERFORM tools.assert( p_table_name IS NOT NULL, 'tools.lookup_table: p_table_name must not be NULL' );
    PERFORM tools.assert( p_object_name IS NOT NULL, 'tools.lookup_table: p_object_name must not be NULL' );
    
    v_params = ARRAY[
      ARRAY['table_name', p_table_name]
      , ARRAY['object_name', p_object_name]
      , ARRAY['schema_name', p_schema_name]
    ];

    -- The lookup table
    EXECUTE tools.parameter_replace(
      $dml$
      CREATE TABLE %schema_name%.%table_name%(
        id integer NOT NULL PRIMARY KEY
        , %object_name% text NOT NULL
        , description text
      );
      $dml$
      , v_params 
    );
    
    -- Get ID
    EXECUTE tools.parameter_replace(
      $dml$
      CREATE FUNCTION %schema_name%.%table_name%__get_id(
        p_%object_name% text
      ) RETURNS integer LANGUAGE sql AS $body$
      SELECT id
      FROM %schema_name%.%table_name%
      WHERE %object_name% = $1
      $body$
      $dml$
      , v_params 
    );

    -- Get text
    EXECUTE tools.parameter_replace(
      $dml$
      CREATE FUNCTION %schema_name%.%table_name%__get(
        p_id integer
      ) RETURNS text LANGUAGE sql AS $body$
      SELECT %object_name%
      FROM %schema_name%.%table_name%
      WHERE id = $1
      $body$
      $dml$
      , v_params 
    );

    -- Getsert ID
    -- +++ this getsert is not concurrent-safe
    EXECUTE tools.parameter_replace(
      $dml$
      CREATE FUNCTION %schema_name%.%table_name%__getsert(
        p_value text
      ) RETURNS integer LANGUAGE plpgsql AS $body$
      DECLARE
        v_id integer := NULL;
      BEGIN
        PERFORM tools.assert(p_value IS NOT NULL, '%schema_name%.%table_name%__getsert: p_value may not be null');
        SELECT id FROM %schema_name%.%table_name% WHERE %object_name% = $1 INTO v_id;
        IF NOT FOUND THEN
          INSERT INTO %schema_name%.%table_name% (id, %object_name%) VALUES (
            coalesce(
              (SELECT max(id) FROM %schema_name%.%table_name%) + 1
              , 1
            )
            , p_value)
          RETURNING id INTO v_id;
        END IF;
        RETURN v_id;
      END;
      $body$
      $dml$
      , v_params
    );
END;
$lookup_table$;
