CREATE OR REPLACE FUNCTION tools.parameter_replace(
    p_template      text
    , p_parameters  text[][]
) RETURNS text LANGUAGE plpgsql AS $parameter_replace$
DECLARE
    v_name text := 'tools.parameter_replace';
    v_out text := p_template;
BEGIN
    PERFORM tools.assert( p_template IS NOT NULL, 'p_template must not be NULL' );
    PERFORM tools.assert( p_parameters IS NOT NULL, 'p_parameters must not be NULL' );
    
    PERFORM tools.assert( array_lower( p_parameters, 2 ) = 1, 'Lower bound of second dimension of p_parameters must be 1' );
    PERFORM tools.assert( array_upper( p_parameters, 2 ) = 2, 'Upper bound of second dimension of p_parameters must be 2' );

    FOR i IN array_lower( p_parameters, 1 ) .. array_upper( p_parameters, 1 ) LOOP
        RAISE DEBUG '%: Replacing % with %', v_name, '%' || p_parameters[i][1] || '%', p_parameters[i][2];
        v_out := replace( v_out, '%' || p_parameters[i][1] || '%', p_parameters[i][2] );
    END LOOP;

    RETURN v_out;
END;
$parameter_replace$;