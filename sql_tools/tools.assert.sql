CREATE OR REPLACE FUNCTION tools.assert(
  p_condition boolean
  , p_message text
) RETURNS boolean SECURITY DEFINER SET search_path FROM CURRENT LANGUAGE plpgsql AS 
$assert$
DECLARE
BEGIN
  IF p_condition THEN
    RETURN true;
  END IF;

  RAISE EXCEPTION '%', p_message;

  RETURN true;
END;
$assert$;
