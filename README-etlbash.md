# Simple psql import script for Bash

## Simple usage

```bash
# This will import checking.csv into scratchpad.checking in my personal database
export PGHOST=localhost
export PGUSER=matt
export PGPASSWORD=*************
export PGDATABASE=matt
./etlbash.sh checking.csv
```

Results executed on my database:

```SQL
DROP TABLE IF EXISTS scratchpad.checking;
CREATE TABLE scratchpad.checking (txn_date text,description text,amount text,balance text);
\COPY scratchpad.checking (txn_date,description,amount,balance) FROM checking.csv WITH QUOTE '\"' DELIMITER ',' CSV;
```

## Advanced usage

given checking.sql:
```SQL
INSERT INTO accounts.checking_history (txn_date, txn_descr, amount, balance)
SELECT c.txn_date::date, c.description, c.amount::numeric, c.balance::numeric
FROM scratchpad.checking;
```

```bash
./etlbash.sh checking.csv
```
will execute checking.sql after

## BACKGROUND

A while back I started a new project at a new job, and I needed a simple way to get CSV data into my database. The files I have contain headers suitable for PostgreSQL column titles and filenames suitable for tables, so I didn't want to touch the data manually if I could help it.

etlbash.sh creates a table with text columns, then executes a `\COPY` statement to import the data. If a custom script is available to perform additional processing (e.g. INSERT...SELECT) that is executed following the import.

As much as I enjoy golfing down my code dependencies, the purpose of keeping this script simple is that it should be portable as bash and psql.

## INPUTS

Runtime dependencies:

* basename
* head
* tr
* mktemp
* rm
* psql

CSV import file:

* header on line 1 contains comma-separated column titles
* data starts on line 2
* input filename must form a suitable table name after (optional) .csv suffix is stripped

SQL:

* custom SQL file must have a .sql suffix, but share the basename of the CSV file
* custom SQL will run after the CSV is imported
* optional

