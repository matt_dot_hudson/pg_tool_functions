#!/bin/bash

# Quick and dirty CSV import for postgres (psql)
# Dependencies: basename, head, tr, mktemp, rm, psql

# Environment variable inputs
# PG* - psql
# ETLBASH_SCHEMA - schema where import table will be created. default is 'scratchpad'.
# ETLBASH_TABLE - table where imported data will be inserted. default is derived from CSV file.
# ETLBASH_POST  - after-import SQL script. default is derived from CSV file with '.sql' appended.
# ETLBASH_CSV - inputs CSV file. default is $1.

# Assumption: psql is authorized via environment vars or dotfile or pg_hba.conf
# Assumption: input CSV has a header row of column titles suitable for postgres
# Assumption: scratchpad schema exists (maybe add CREATE IF NOT EXISTS?)

# Positive test: psql works, CSV imports, after-import script runs
# Positive test: psql works, CSV imports, no after-import script exists
# Positive test: psql works, CSV contains exactly one column, after-import script runs
# Negative test: PGDATABASE is not set
# Negative test: psql can't connect
# Negative test: psql doesn't exist
# Negative test: CSV file doesn't exist
# Negative test: CSV file contains garbage
# Negative test: CSV file does not have a header

ETLBASH_SCHEMA=${ETLBASH_SCHEMA:-'scratchpad'}
ETLBASH_TABLE=${ETLBASH_TABLE:-$(basename "$1" .csv)}
ETLBASH_POST=${ETLBASH_POST:-$(dirname "$1")/$ETLBASH_TABLE.sql}
ETLBASH_CSV=${ETLBASH_CSV:-"$1"}

# Check input file
if [ ! -f "$1" ]; then
	echo "Can't find $1"
	echo
	exit
fi

# Check database parameter
if [ -z "$PGDATABASE" ]; then
	echo "PGDATABASE not set"
	echo
	exit
fi

cols=$(head -1 "$ETLBASH_CSV" | tr '[:upper:]' '[:lower:]' | tr -d '# ')

echo '========== Attempting to Import ============'
echo "Input: $ETLBASH_CSV"
echo "Schema: $ETLBASH_SCHEMA"
echo "Table: $ETLBASH_TABLE"
echo "Parsed Columns: $cols"
echo

echo '========== Import ============'
cols_types="${cols//","/" text,"} text"
tablescript="DROP TABLE IF EXISTS $ETLBASH_SCHEMA.$ETLBASH_TABLE CASCADE;"
tablescript+=$'\n'"CREATE UNLOGGED TABLE $ETLBASH_SCHEMA.$ETLBASH_TABLE ($cols_types);"
tablescript+=$'\n'"\COPY $ETLBASH_SCHEMA.$ETLBASH_TABLE ($cols) FROM $ETLBASH_CSV WITH QUOTE '\"' DELIMITER ',' CSV;"

# Robust tempfile usage via deprecated 'tempfile' manpage
# mktemp is recommended now
tempsql=$(mktemp -p /tmp "etlbash_$ETLBASH_TABLE.XXXXXX.sql") || exit
# shellcheck disable=SC2064
trap "rm -rf -- '$tempsql'" EXIT

# Write SQL script
echo "$tablescript" > "$tempsql"
# Import data
psql -f "$tempsql"

# Execute after-import script if there is one
if [ -f "$ETLBASH_POST" ]; then
	echo '========== After-Import ============'
	psql -f "$ETLBASH_POST"
fi

rm -rf -- "$tempsql"
trap - EXIT
exit
