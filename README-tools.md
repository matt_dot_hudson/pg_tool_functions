# tools schema

Postgres functions to make coding in SQL a little more convenient

## assert(condition boolean, message text)

Does what it says on the tin

## exec(command text)

Does what it says on the tin using EXECUTE

## exec(command text, role text)

Does what it says on the tin using EXECUTE. Calls SET LOCAL ROLE [role] first.

## parameter_replace(template text, parameters text[][])

Replaces instances of %parameter% with values looked up in the parameters array
